{-# LANGUAGE OverloadedStrings #-}
module Main where

import qualified Data.ByteString as B hiding (putStrLn)
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import Data.Monoid ((<>))
import Control.Monad (when, forM_)
import System.FilePath ((</>))

import Control.Lens

import Notebook as N
import Utils
import Formats

import Options.Applicative as Opt hiding (command)

langTag :: T.Text -> N.Language
langTag command = if command `safeIndex` 0 == Just '%'
                  then parseLanguage $ T.drop 1 (head (T.lines command))
                  else N.Spark N.Scala
  where parseLanguage "scala"        = N.Scala
        parseLanguage "python"       = N.Python
        parseLanguage "r"            = N.R
        parseLanguage "java"         = N.Java
        parseLanguage "haskell"      = N.Haskell
        parseLanguage "spark-scala"  = N.Spark N.Scala
        parseLanguage "spark-python" = N.Spark N.Python
        parseLanguage "spark-r"      = N.Spark N.R
        parseLanguage "md"           = N.Markdown
        parseLanguage o              = N.Other o

prependCode :: B.ByteString -> N.Notebook -> N.Notebook
prependCode c = over nCommands (codeBlock :)
  where codeBlock     = C (langTag (E.decodeUtf8 c)) (dropFirstLine $ E.decodeUtf8 c) Nothing False False
        dropFirstLine code
          | code `safeIndex` 0 == Just '%' = T.unlines . tail . T.lines $ code
          | otherwise                      = code

prependCodes :: B.ByteString -> Either String [(String, N.Notebook)] -> Either String [(String, N.Notebook)]
prependCodes c nList = over (each . _2) (prependCode c) <$> nList

format :: Parser NotebookFormat
format = parseFormat <$> format'
  where parseFormat "databricks-json" = databricksJSONFormat
        parseFormat "zeppelin"        = zeppelinFormat
        parseFormat _                 = error "Unknown target format"
        format' = strOption ( long "format"
                              <> short 'f'
                              <> metavar "FORMAT"
                              <> help "Format of the notebook" )

inputPaths :: Parser [FilePath]
inputPaths = some (Opt.argument str (metavar "INPUT_NOTEBOOKS"))

outputPath :: Parser FilePath
outputPath = strOption (long "out" <> short 'o' <> metavar "OUTPUT")

inputCode :: Parser FilePath
inputCode = strOption (long "code" <> short 'c' <> metavar "CODE_FILE" <> help "File containing code to prepend")

data Run = Run { inputFormat :: NotebookFormat, codeFile :: FilePath, inputs :: [FilePath], output :: Maybe FilePath }

run :: Parser Run
run = Run <$> format <*> inputCode <*> inputPaths <*> optional outputPath

opts :: ParserInfo Run
opts = info (run <**> helper)
  ( fullDesc
  <> progDesc "Adds the contents of the provided text file as the first cell of the provided notebook." )

main  :: IO ()
main = do
  (Run (from, to) codefile inputs output) <- execParser opts
  inputStreams <- if null inputs
                  then do stream <- BL.getContents
                          return [("stdin", stream)]
                  else do streams <- mapM BL.readFile inputs
                          return (zip inputs streams)
  code <- B.readFile codefile
  let attempt = to <$> concatMapM (prependCodes code . uncurry from) inputStreams
      results  = either (error . show) id attempt

  when (null results) (error "Nothing to output.")

  case output of
    Nothing -> case results of
      [(_, x)] -> BL.putStrLn x
      _ -> error "Cannot output multiple files to stdout"
    Just o -> forM_ results $ \(f, x) -> do
      ensureCanBeCreated (o </> f)
      BL.writeFile (o </> f) x
