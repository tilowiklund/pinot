{-# LANGUAGE OverloadedStrings #-}
module Main where

import qualified Data.ByteString.Lazy as B hiding (putStrLn)
import qualified Data.ByteString.Lazy.Char8 as B (putStrLn)
import Data.Monoid ((<>))
import Control.Monad (when, forM_, liftM)
import Data.Either (partitionEithers)
import System.FilePath ((</>))

import Utils
import Formats

import Options.Applicative as Opt

sourceFormat :: Parser SourceFormat
sourceFormat = parseFormat <$> sourceFormat'
  where parseFormat "databricks"      = databricksDBCSource
        parseFormat "databricks-json" = databricksJSONSource
        parseFormat "zeppelin"        = zeppelinSource
        parseFormat _ = error "Unknown source format"
        sourceFormat' = strOption ( long "from"
                                  <> short 'f'
                                  <> metavar "FROM"
                                  <> help "Format to convert from" )

targetFormat :: Parser TargetFormat
targetFormat = parseFormat <$> targetFormat'
  where parseFormat "databricks-json" = databricksJSONTarget
        parseFormat "zeppelin"        = zeppelinTarget
        parseFormat "markdown"        = markdownTarget
        parseFormat "markdown-katex"  = markdownTargetKaTeX
        parseFormat "html"            = htmlTarget
        parseFormat "pandoc"          = pandocTarget
        parseFormat "pandoc-json"     = pandocJSONTarget
        parseFormat "jupyter"         = jupyterTarget
        parseFormat "jupyter-book"    = jupyterBookTarget
        parseFormat "mdbook"          = mdBookTarget
        parseFormat _ = error "Unknown target format"
        targetFormat' = strOption ( long "to"
                                    <> short 't'
                                    <> metavar "TO"
                                    <> help "Format to convert to" )

inputPaths :: Parser [FilePath]
inputPaths = some (Opt.argument str (metavar "INPUTS..."))

outputPath :: Parser FilePath
outputPath = strOption (long "out" <> short 'o' <> metavar "OUTPUT")

data OnError = Fail
             | KeepGoing

onErrorOpt :: Parser OnError
onErrorOpt = flag Fail KeepGoing (long "keep-going" <> short 'c' <> help "Continue with next input in case of failure")

extraResourcePathOpt :: Parser [FilePath]
extraResourcePathOpt = fmap maybeSingleton $ optional $ strOption (long "extra-resource-path" <> short 'R' <> help "additional path to search for resources (images, ...)")
  where maybeSingleton = maybe [] singleton
        singleton x = [x]

data Run = Run { from :: SourceFormat
               , to :: TargetFormat
               , inputs :: [FilePath]
               , output :: Maybe FilePath
               , onError :: OnError
               , extraResourcePath :: [FilePath] }

run :: Parser Run
run = Run <$> sourceFormat <*> targetFormat <*> inputPaths <*> optional outputPath <*> onErrorOpt <*> extraResourcePathOpt

opts :: ParserInfo Run
opts = info (run <**> helper)
  ( fullDesc
  <> progDesc "Convert between different notebook formats" )

main  :: IO ()
main = do
  (Run from to inputs output keepGoing ep) <- execParser opts
  inputStreams <- if null inputs
                  then do stream <- B.getContents
                          return [("stdin", stream)]
                  else do streams <- mapM B.readFile inputs
                          return (zip inputs streams)
  --uncurry from
  -- attempt <- to =<< concatMapM (concatMapM (uncurry from)) inputStreams

  -- let results  = either (error . show) id _attempt

  (failedParses, successfulParses) <- liftM partitionEithers $ mapM (uncurry (from ep)) inputStreams

  forM_ failedParses $ \f -> do
    putStr "Error parsing:"
    print f

  successes <- mapM (to ep) successfulParses

  when (null successes) (error "No successful output")

  case output of
    Nothing -> case successes of
      [[(_, x)]] -> B.putStrLn x
      _ -> error "Cannot output multiple files to stdout"
    Just o -> forM_ successes $ \ss -> forM_ ss $ \(f, x) -> do
      ensureCanBeCreated (o </> f)
      B.writeFile (o </> f) x
